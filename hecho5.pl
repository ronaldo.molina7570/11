%hechos
padrede(martin,luis).
padrede(luis,jose).
padrede(luis,pedro).
espadre(martin).
espadre(luis).

%reglas
hijode(B,A):-padrede(A,B).

%reglas_and
abuelode(A,C):-padrede(A,B),padrede(B,C).
hermanos(B,C):-espadre(A),padrede(A,B),padrede(A,C).

%reglas_or
familiarde(A,B):-padrede(A,B);hijode(A,B);hermanode(A,B);abuelode(A,B).