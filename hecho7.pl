:- use_module(library(pce)).
:- use_module(library(pce_style_item)).
main:-
new(Menu, dialog('Sistema Experto Medico', size(500,500))),
new(L, label(nombre, 'BIENVENIDOS')),
new(@texto,label(nombre, 'Proceda a responder las siguientes preguntas para obtener su diagnostico:')),
new(@respl, label(nombre,'')),
new(Salir, button('Salir', and(message(Menu,destroy), message(Menu, free)))),
new(@boton,button('�Desea continuar con el test?', message(@prolog,botones))),

send(Menu, append(L)), new(@btncarrera, button('�Diagnostico?')),
send(Menu,display,L,point(100,20)),
send(Menu,display,@boton,point(130,150)),
send(Menu,display,@texto,point(50,100)),
send(Menu,display,Salir,point(20,400)),
send(Menu,display,@respl,point(20,130)),
send(Menu,open_centered).
